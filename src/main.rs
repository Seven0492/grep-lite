fn main() {
    let search_term = "picture";
    
    let quote = "\
    Every face, every shop, bedroom window, public-house, and\
    \ndark square is a picture feverishly turned--in search of what?\
    \nIt is the same with books.\
    \nWhat do we seek trough millions of pages?";
    
    for (line_number, line) in quote.lines().enumerate() {
        if line.contains(&search_term) {
            println!("{}: {}", &line_number + 1, &line);
        }
    }
}
